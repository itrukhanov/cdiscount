# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor


class CdiscountSpider(scrapy.Spider):
    name = 'cdiscount'
    allowed_domains = ['cdiscount.com']
    start_urls = ['http://cdiscount.com']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.visited_urls = set()

    def parse(self, response):
        self.visited_urls.add(response.url)

        link_extractor = LinkExtractor()
        links = (link.url for link in link_extractor.extract_links(response)
                 if link.url not in self.visited_urls)

        attributes_selector = response.css('table.fpDescTb')
        if attributes_selector.extract_first():
            original_price = response.css('div.fpPriceBloc .fpStriked::text').extract_first()
            promo_price = response.css('.fpPrice::attr(content)').extract_first()
            sold_by = response.css('a.fpSellerName::text').extract_first()

            attributes_dict = {}
            trs = attributes_selector.css('tr')
            ths = (th for th in (tr.css('th') for tr in trs) if th)
            tds = (td if td else next(ths)
                   for td in (tr.css('td') for tr in trs))

            header = next(tds).css('*::text').extract_first()
            attributes_dict[header] = {}
            for td in tds:
                if len(td) == 1:
                    [header] = td
                    header = header.css('*::text').extract_first()
                    attributes_dict[header] = {}
                    continue

                (key, value) = td
                key = key.css('*::text').extract_first()
                value = ' '.join(value.css('*::text').extract())

                attributes_dict[header][key] = value

            yield {
                'info': {
                    'Title': response.css('h1[itemprop="name"]::text').extract_first(),
                    'Brand': next(cat for cat in (attributes_dict.values()) if 'Marque' in cat)['Marque'],
                    'State': response.css('span.fpProductCondition::text').extract_first(),
                    'Short description': response.css('p[itemprop="description"]::text').extract_first(),
                    'Url images': response.css('a[itemprop="image"]::attr(href)').extract_first(),
                    'Promo price': promo_price if original_price else original_price,
                    'Original price': original_price.strip() if original_price else promo_price,
                    'Category': response.css('#bc span[itemprop="title"]::text').extract(),
                    'Url categories': response.css('#bc a[itemprop="url"]::attr(href)').extract(),
                    'Economy': response.css('div.jsFpAjaxEconomy div.ecoBlk span::text').extract_first(),
                    'Eco-participation': response.css('p.fpEcoTx a::text').extract_first(),
                    'Stock': response.css('.fpProductAvailability::text').extract_first(),
                    'Stars': response.css('span[itemprop="ratingValue"]::text').extract_first(),
                    'Points forts': response.css('div.fpBulletPointReadMore li::text').extract(),
                    'Sold by': sold_by if sold_by else response.css('span.logoCDS::text').extract_first(),
                },
                'attributes': attributes_dict
            }

        for link in links:
            yield response.follow(link, self.parse)
