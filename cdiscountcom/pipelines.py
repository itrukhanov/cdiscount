# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import csv
import os


class CdiscountcomPipeline(object):
    def process_item(self, item, spider):
        info_name = 'products_info'
        attrs_name = 'products_attributes'

        info = item['info']
        attributes = item['attributes']

        info = {k: v.strip() if type(v) is str else v for k, v in info.items()}
        attributes = {k.strip(): v for k, v in attributes.items()}

        name = info['Title'].replace('/', '.')

        os.makedirs(info_name, exist_ok=True)
        os.makedirs(attrs_name, exist_ok=True)
        info_path = os.path.join(info_name, name + '.csv')
        attrs_path = os.path.join(attrs_name, name + '.csv')

        with open(info_path, 'w') as f:
            writer = csv.writer(f)
            writer.writerow(info.keys())
            writer.writerow(info.values())

        with open(attrs_path, 'w') as f:
            writer = csv.writer(f)
            writer.writerow((
                'product_attribute_set_num',
                'product_attribute_set',
                'product_attribute_num',
                'product_attribute',
                'attribute_value',
            ))

            for i, v in enumerate(attributes):
                line = [i + 1, v]
                for i, (k, v) in enumerate(attributes[v].items()):
                    writer.writerow(line + [i + 1, k, v])
                    line = ['', '']
